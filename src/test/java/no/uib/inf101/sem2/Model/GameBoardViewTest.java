package no.uib.inf101.sem2.Model;
import org.junit.jupiter.api.Test;
import no.uib.inf101.sem2.GameDisplay.GameBoardView;
import no.uib.inf101.sem2.GameLogic.GameArena;
import no.uib.inf101.sem2.GameLogic.GameMechanics;
import no.uib.inf101.sem2.GameLogic.Snake;

import static org.junit.jupiter.api.Assertions.*;

class GameBoardViewTest {

    @Test
    void testUpdateGame() {
       
        Snake snake = new Snake(new GameArena(20, 20));
        GameArena board = new GameArena(20, 20);
        GameMechanics mechanics = new GameMechanics(board);
        GameBoardView view = new GameBoardView(snake, board, mechanics);

        
        view.updateUI();

        
        assertNotNull(view);
        
    }
}
