package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.GameLogic.GameMechanics;
import no.uib.inf101.sem2.GameLogic.GameArena;
import no.uib.inf101.sem2.Grid.CellPosition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnakeTest {

    @Test
    void shouldSnakeMove() {
        GameArena board = new GameArena(10, 10);
        GameMechanics model = new GameMechanics(board);

        model.getSnake().getSnakeBody().add(new CellPosition(0, 0));
        model.getSnake().getSnakeBody().add(new CellPosition(0, 1));


        model.getSnake().move();

        assertEquals(new CellPosition(1, 1), model.getSnake().getSnakeBody().get(0));
        assertEquals(new CellPosition(0, 1), model.getSnake().getSnakeBody().get(1));

    }
}