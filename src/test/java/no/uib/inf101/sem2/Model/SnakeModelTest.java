package no.uib.inf101.sem2.Model;

import no.uib.inf101.sem2.GameLogic.Apple;
import no.uib.inf101.sem2.GameLogic.GameMechanics;
import no.uib.inf101.sem2.GameLogic.GameArena;
import no.uib.inf101.sem2.Grid.CellPosition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnakeModelTest {

    @Test
    void shouldCollideWithEdge() {
        GameArena board = new GameArena(10, 10);
        GameMechanics model = new GameMechanics(board);

        // Test snake at the edge of board
        model.getSnake().getSnakeBody().set(0, new CellPosition(10, 10));
        assertEquals(true, model.checkWallCollision());
    }

    @Test
    void placingSnakeOnboard() {
        GameArena board = new GameArena(10, 10);
        GameMechanics model = new GameMechanics(board);

        // Test snake inside the board
        model.getSnake().getSnakeBody().set(0, new CellPosition(5, 5));
        assertEquals(false, model.checkWallCollision());

    }

    @Test
    void testWallCollisionOutside() {
        GameArena board = new GameArena(10, 10);
        GameMechanics model = new GameMechanics(board);

        // Test snake outside the board
        model.getSnake().getSnakeBody().set(0, new CellPosition(-1, 5));
        assertEquals(true, model.checkWallCollision());
    }


    @Test
    void testFoodCollision() {
        GameArena board = new GameArena(10, 10);
        GameMechanics model = new GameMechanics(board);
        Apple apple = new Apple(10, 10);

        // Add a few cells to the snake's body
        model.getSnake().getSnakeBody().add(new CellPosition(0, 0));
        model.getSnake().getSnakeBody().add(new CellPosition(0, 1));
        model.getSnake().getSnakeBody().add(new CellPosition(0, 2));

        // Place the apple in the same position as the snake's head
        apple.setAppPos(new CellPosition(0, 2));

        // Move the snake's head away from the apple and call the method again
        model.getSnake().getSnakeBody().remove(0);
        model.getSnake().getSnakeBody().add(new CellPosition(1, 2));

        assertFalse(model.checkAppleCollision());

    }

    @Test
    void testSelfCollision() {
        GameArena board = new GameArena(10, 10);
        GameMechanics model = new GameMechanics(board);

        // Add a few cells to the snake's body
        model.getSnake().getSnakeBody().add(new CellPosition(5, 5));
        model.getSnake().getSnakeBody().add(new CellPosition(5, 4));
        model.getSnake().getSnakeBody().add(new CellPosition(5, 3));
        // Set the snake's head to collide with its own body
        model.getSnake().getSnakeBody().set(0, new CellPosition(5, 3));

        assertEquals(true, model.checkSelfCollision());
    }
}