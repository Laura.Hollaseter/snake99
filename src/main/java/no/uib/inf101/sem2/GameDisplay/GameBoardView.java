package no.uib.inf101.sem2.GameDisplay;


import javax.swing.*;

import no.uib.inf101.sem2.GameLogic.*;
import no.uib.inf101.sem2.Grid.CellPosition;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class GameBoardView extends JPanel {
    private final ColorTheme colorTheme;
    private Snake snake;
    private GameArena board;
    private GameMechanics snakeModel;
    final int cellSize = 25;

    public GameBoardView(Snake snake, GameArena board, GameMechanics model) {
        this.board = board;
        this.snakeModel = model;
        this.snake = snake;
        this.colorTheme = new DefaultColorTheme();
        this.setBackground(colorTheme.getBackgroundColor());
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(500, 500));
}


/**
setColor og fillRect tømmer skjermen
if løkken sjekker spillets tilstand
*/
   

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
    
        g2d.setColor(Color.black);
        g2d.fillRect(0, 0, getWidth(), getHeight());
    
        if (snakeModel.getGameState() == PlayState.GAME_OVER) {
            gameOver(g2d);
            drawScore(g2d);
        } else {
            drawGame(g2d);
            drawScore(g2d);
            drawSnake(g2d);
            drawApple(g2d);
        }
    }
    

    /**

     Draws the snake on the graphics object using the BLUE color for each of its cells.
     @param g The graphics object to draw on.
     */
    
    private void drawSnake(Graphics g) {
        g.setColor(Color.BLUE); // Set color once before the loop
        for (CellPosition pos : snake.getSnakeBody()) {
            int x = pos.col() * cellSize;
            int y = pos.row() * cellSize;
            g.fillRect(x, y, cellSize, cellSize); // Use calculated values directly
        }
    }
    

    /**
     Draws the apple on the graphics object using the red color for the oval shape.
     @param g The graphics object to draw on.
     */

     private void drawApple(Graphics g) {
        g.setColor(Color.RED); 
        CellPosition applePos = snakeModel.getApple().getAppPos(); 
        int xPos = applePos.col() * cellSize; 
        int yPos = applePos.row() * cellSize; 
        g.fillOval(xPos, yPos, board.rows(), board.cols()); 
    }
    
    

    /**
     Draws the game board and its components on the graphics object. This includes the frame, the snake, and the apple.
     @param g The graphics object to draw on.
     (tegner rammen rundt spillbrettet.)
     */
    private void drawGame(Graphics2D g) {
        double margin = 2; //Setter en margin på 2 piksler
        double x = margin; //Setter startposisjonen for x-koordinaten
        double y = margin; //Setter startposisjonen for y-koordinaten
        //trenger bredde og høyde for å lage rektangelet
        double width = this.getWidth() - 2 * margin;//Beregner bredden på rektangelet
        double height = this.getHeight() - 2 * margin;//Beregner høyden på rektangelet
        //bruker alt vi har regnet ut for å lage selve rektangelet/bameboard
        Rectangle2D rect = new Rectangle2D.Double(x, y, width, height);

        g.setColor(colorTheme.getFrameColor());
        g.fill(rect);

    }
    /**
     Draws the "Game Over" text in red font on the graphics object when the game is over.
     @param g The graphics object to draw on.
     */
    private void gameOver (Graphics graphics) {
        graphics.setColor(Color.red);
        graphics.setFont(new Font("Ink Free", Font.BOLD, 75));
        FontMetrics fontMetrics = getFontMetrics(graphics.getFont());
        int centerY = (getHeight() - fontMetrics.getHeight()) / 2 + fontMetrics.getAscent();
        int centerX = (getWidth() - fontMetrics.stringWidth("Game Over")) / 2;
        graphics.drawString("Game Over", centerX, centerY);
    }


    
    /**
     Draws the score panel showing the player's score in red font on the graphics object.
     @param g The graphics object to draw on.
     */
    private void drawScore(Graphics graphics) {
        graphics.setColor(Color.white);
        graphics.setFont(new Font("Ink Free", Font.BOLD, 40));
        FontMetrics scoreMetrics = getFontMetrics(graphics.getFont());
        int scoreX = (getWidth() - scoreMetrics.stringWidth("Score: " + snakeModel.appleEaten)) / 2;
        int scoreY = graphics.getFont().getSize();
        graphics.drawString("Score: " + snakeModel.appleEaten, scoreX, scoreY);
    }

    }




