package no.uib.inf101.sem2.GameDisplay;



import java.awt.*;

public interface ColorTheme {

    Color getFrameColor ();

    Color getBackgroundColor ();
}
