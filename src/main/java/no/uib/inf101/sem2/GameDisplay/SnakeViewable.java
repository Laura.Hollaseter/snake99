package no.uib.inf101.sem2.GameDisplay;


import no.uib.inf101.sem2.GameLogic.GameArena;
import no.uib.inf101.sem2.Grid.GridCell;
import no.uib.inf101.sem2.Grid.GridDimension;

public interface SnakeViewable {


    /**
     * Returns the dimension of the grid.
     * @return the dimension of the grid as a {@code GridDimension} object
     */
    GridDimension getDimension();

}
