package no.uib.inf101.sem2.ControlCenter;



import javax.swing.*;

import no.uib.inf101.sem2.GameDisplay.GameBoardView;
import no.uib.inf101.sem2.GameLogic.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
Controller-klassen fungerer som broen mellom brukerinput og spillets logikk
Initialiserer kontrolleren ved å sette opp spillet, og brukerinterfacet
Binder event listeners til snakeView, så spillet kan motta tastetrykk
kaller på startGame() for å starte spillet.
*/

public class Controller implements KeyListener, ActionListener {
    private static final int DELAY = 100;
    private GameBoardView snakeView;
    private Snake snake;
    private Timer timer;
    private GameMechanics snakeModel;
    private PlayState gameState;


    public Controller(GameBoardView snakeView, Snake snake, GameMechanics snakeModel) {
        this.snake = snake;
        this.snakeView = snakeView;
        this.snakeModel = snakeModel;
        snakeView.addKeyListener(this);
        snakeView.setFocusable(true);
        snakeView.requestFocus();
        startGame();
    }

    /**
     Starts the game timer.
     Creates a new Timer object with the delay specified 
     in the DELAY constant and starts it.
     */
    protected void startGame() {
        timer = new Timer(DELAY, this);
        timer.start();
    }

 
/**
må være der fordi KeyListener-grensesnittet krever det, 
men de er ikke i bruk

keyTyped(), keyPressed(), keyReleased(), og actionPerformed() 
må være public fordi de overrider public metoder definert i KeyListener og ActionListener grensesnittene. 
Disse metodene må være tilgjengelige for Java-event-systemet.
*/
    @Override
    public void keyTyped(KeyEvent e) {
    }


/**
reagerer på tastetrykk. Oppdaterer slangens retning basert på hvilken tast 
brukeren trykker på, forhindrer at slangen kan snu direkte bakover, 
og ber om en oppdatering av spillvinduet.

*/


    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if (snake.getDirection() != Orientation.RIGHT) {
                    snake.setDirection(Orientation.LEFT);
                    break;
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (snake.getDirection() != Orientation.LEFT) {
                    snake.setDirection(Orientation.RIGHT);
                    break;
                }
            case KeyEvent.VK_UP:
                if (snake.getDirection() != Orientation.DOWN) {
                    snake.setDirection(Orientation.UP);
                    break;
                }
            case KeyEvent.VK_DOWN:
                if (snake.getDirection() != Orientation.UP) {
                    snake.setDirection(Orientation.DOWN);
                    break;
                }
        }
        snakeView.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(getGameScreen() == PlayState.GAME_OVER) {
            snakeView.repaint();
        }

    }


/**
utføres hver gang timeren utløper. Metoden flytter slangen, sjekker for kollisjoner, 
og oppdaterer spilltilstanden til GAME_OVER 
hvis nødvendig. Deretter oppdaterer den spillvinduet med den nye tilstanden.

actionPerformed må forbli public på grunn av dens rolle i event-håndtering i Swing.
 Event-systemet i Java forventer at denne metoden er tilgjengelig på dette tilgangsnivået 
 for å kunne utføre sine funksjoner korrekt. Endringer i tilgangsnivået til denne metoden vil 
 ikke bare bryte med kontrakten etablert av ActionListener-grensesnittet, men også potensielt 
 føre til runtime-feil hvis systemet ikke lenger kan finne og kalle metoden når en handling finner sted.
*/

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (getGameScreen() == PlayState.ACTIVE_GAME) {
            snake.move();
            if (snakeModel.checkWallCollision()) {
                timer.stop();
                this.gameState = PlayState.GAME_OVER;
            } else if (snakeModel.checkAppleCollision()) {
                snakeModel.getSnake().expand();
            } else if (snakeModel.checkSelfCollision()){
                timer.stop();
                this.gameState = PlayState.GAME_OVER;
            }
        }
        snakeView.repaint();
        snakeModel.setGameScreen(gameState);

    }

    /**
     * Returns the current state of the game screen.
     * @return the current state of the game screen
     */
    protected PlayState getGameScreen() {
        return PlayState.ACTIVE_GAME;
}

}


