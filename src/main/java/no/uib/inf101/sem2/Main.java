package no.uib.inf101.sem2;

import no.uib.inf101.sem2.ControlCenter.Controller;
import no.uib.inf101.sem2.GameDisplay.GameBoardView;
import no.uib.inf101.sem2.GameLogic.GameMechanics;
import no.uib.inf101.sem2.GameLogic.Snake;
import no.uib.inf101.sem2.GameLogic.GameArena;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {

    GameArena snakeBoard = new GameArena(20, 20);
    Snake snake = new Snake(snakeBoard);
    GameMechanics snakeModel = new GameMechanics(snakeBoard);
    GameBoardView view = new GameBoardView(snakeModel.getSnake(), snakeBoard, snakeModel);
    Controller controller = new Controller(view, snakeModel.getSnake(), snakeModel);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Snake");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


  }
}
