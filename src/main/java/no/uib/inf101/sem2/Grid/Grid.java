package no.uib.inf101.sem2.Grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
*List<List<E>> lager rutenettet, noe som gjør det mulig å lagre et hvilket som helst objekt i hver celle. 
*Generisk typen E gjør klassen gjenbrukbar for mange forskjellige typer objekter 
*som måtte være nødvendige å lagre i et rutenett. 

*En konstruktør som oppretter et nytt rutenett med angitte antall rader og kolonner. 
*Alle celler vil være satt til null. 
*/
public class Grid<E> implements  IGrid<E> {
    private final List<List<E>> grid;
    private final int rows, cols;


    public Grid (int rows, int cols) {
        this(rows, cols, null);
    }


/**
*Enda en konstruktør som også tar et initialverdi-argument. 
*Dette oppretter et rutenett der hver celle er initialisert til initialvalue   
*/

    public Grid(int rows, int cols, E initialvalue) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>();


        for (int row = 0; row < rows; row++) {
            List<E> allRows = new ArrayList<>();
            for (int col = 0; col < cols; col++) {
                allRows.add(initialvalue);
            }
            this.grid.add(allRows);
        }
    }

/**
*Oppdaterer verdien til rutenettet på en gitt CellPosition til den angitte verdien value.
*/
    @Override
    public void set(CellPosition pos, E value) {
        this.grid.get(pos.col()).set(pos.row(), value);
    }

/**
*Returnerer verdien som er lagret på en spesifikk CellPosition i rutenettet. 
*Hvis posisjonen ikke er på rutenettet, kaster den IndexOutOfBoundsException.
*/
    @Override
    public E get(CellPosition pos) {
        if (!positionIsOnGrid(pos)) {
            throw new IndexOutOfBoundsException();
        }
        return this.grid.get(pos.col()).get(pos.row());
    }

/**
*Sjekker om en gitt CellPosition er innenfor grensene av rutenettet.
*/
    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        return pos.col() >= 0 && pos.col() < rows() && pos.row() >= 0 && pos.row() < cols();
    }

/**
*tillater iterasjon gjennom alle cellene i rutenettet
*For hver posisjon på rutenettet, opprettes en GridCell som holder posisjonen 
*og verdien til cellen, og legger dette til i en liste som iterator returnerer.
*/
    @Override
    public Iterator iterator() {
        ArrayList<GridCell<E>> gridCells = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                CellPosition cellPosition = new CellPosition(row, col);
                E item = get(cellPosition);
                GridCell<E> gridCell = new GridCell<>(cellPosition, item);
                gridCells.add(gridCell);
            }
        }
        return gridCells.iterator();
    }


/**
Returnerer antall rader i rutenettet.
*/
    @Override
    public int rows() {
        return rows;
    }

/**
Returnerer antall kolonner i rutenettet.
*/
    @Override
    public int cols() {
        return cols;
    }
}

