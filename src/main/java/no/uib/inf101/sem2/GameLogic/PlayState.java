package no.uib.inf101.sem2.GameLogic;
/**
 * This class is an enum and keeps track of the current
 * state of the game.
 */

 public enum PlayState {
    ACTIVE_GAME, GAME_OVER
}
