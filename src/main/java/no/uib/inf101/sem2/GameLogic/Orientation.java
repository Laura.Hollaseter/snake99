package no.uib.inf101.sem2.GameLogic;

/**
 * This class represent the different directions that
 * the snake can move in.
 */

public enum Orientation {
    UP, DOWN, LEFT, RIGHT
}
