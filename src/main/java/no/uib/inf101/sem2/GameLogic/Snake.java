package no.uib.inf101.sem2.GameLogic;


import java.util.ArrayList;

import no.uib.inf101.sem2.GameDisplay.SnakeViewable;
import no.uib.inf101.sem2.Grid.CellPosition;


/**
 * This class represent the snake itself. It
 * handle's the snake position, length and direction.
 */

public class Snake {
    private ArrayList<CellPosition> body;
    private Orientation direction;
    private GameArena board;

    public Snake(GameArena board) {
        this.board = board;
        this.direction = Orientation.RIGHT;
        this.body = new ArrayList<>();
        body.add(new CellPosition(0, 1)); // Setter lokasjonen til snaken i punktet 0, 0.
        body.add(new CellPosition(1, 1));
    }
    /**
     * Gets the body of the snake
     *
     * @return Returns the body of the snake
     */
    public ArrayList<CellPosition> getSnakeBody() {
        return body;
    }

    /**
     Moves the snake by updating the positions of all its segments. The head moves in the direction determined by
     the current direction, and each subsequent segment is moved to the position of the segment that preceded it.
     */
    public void move() {
        CellPosition head = body.get(0);
        for (int i = body.size() -1; i > 0; i--) {
            CellPosition prevSegmnet = body.get(i - 1);
            body.set(i, prevSegmnet);
        }

        switch (direction) {
            case UP:
                head = new CellPosition(head.col(), head.row() - 1);
                break;
            case DOWN:
                head = new CellPosition(head.col(), head.row() + 1);
                break;
            case LEFT:
                head = new CellPosition(head.col() - 1, head.row());
                break;
            case RIGHT:
                head = new CellPosition(head.col() + 1, head.row());
                break;
            default:
                throw new IllegalArgumentException();
        }
        body.set(0, new CellPosition(head.col(), head.row()));
    }
    /**
     * When a snake eats an apple the body of the snake will grow with
     * one part.
     */
    public void expand() {
        CellPosition lastPart = body.get(body.size() - 1);
        body.add(new CellPosition(lastPart.col(), lastPart.row() ));
    }
    /**
     Returns the current direction of the snake.
     @return the current direction of the snake
     */
    public Orientation getDirection() {
        return direction;
    }
    /**
     Sets the direction of the snake.
     @param direction the direction to be set for the snake
     */
    public void setDirection(Orientation direction) {
        this.direction = direction;
    }

}




