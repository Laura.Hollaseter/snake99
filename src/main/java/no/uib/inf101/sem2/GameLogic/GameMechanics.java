package no.uib.inf101.sem2.GameLogic;


import no.uib.inf101.sem2.GameDisplay.SnakeViewable;
import no.uib.inf101.sem2.Grid.CellPosition;
import no.uib.inf101.sem2.Grid.Grid;
import no.uib.inf101.sem2.Grid.GridDimension;

/**
 * This class handles the game logic. It contains methods for
 * updating the game state and handling collisions.
 */
public class GameMechanics implements SnakeViewable {
    private GameArena snakeBoard;
    private Snake snake;
    private Apple apple;
    private PlayState gameScreen = PlayState.ACTIVE_GAME;
    public int appleEaten = 0;

    public GameMechanics(GameArena snakeBoard) {
        this.snakeBoard = snakeBoard;
        this.snake = new Snake(snakeBoard);
        this.apple = new Apple(snakeBoard.rows(), snakeBoard.cols());
    }

    /**
     Returns the current snake object.
     @return the current snake object
     */
    public Snake getSnake() {
        return snake;
    }


    @Override
    public GridDimension getDimension() {
        return new Grid<>(snakeBoard.rows(), snakeBoard.cols());
    }

    /**
     * This method checks if the snake collides with the
     * boarders of the game.
     * @return Returns true if the snake dosent collide.
     * Returns false if the snake collides with the boardes.
     */
    public boolean checkWallCollision() {
        CellPosition head = snake.getSnakeBody().get(0);
        int x = head.col();
        int y = head.row();

        if (x < 0 || x >= this.snakeBoard.cols()
                || y < 0 || y >= this.snakeBoard.rows()) {
            return true;
        }
        return false;
    }

    /**
     Checks if the snake's head collides with the apple.
     @return true if the snake's head collides with the apple, false otherwise
     */
    

    public boolean checkAppleCollision() {
        CellPosition head = getSnake().getSnakeBody().get(0);
        CellPosition applePosition = apple.getAppPos();
        
        boolean hasCollidedWithFood = head.equals(applePosition);
        if (hasCollidedWithFood) {
            consumeApple();
        }
        
        return hasCollidedWithFood;
    }
    
    private void consumeApple() {
        appleEaten++;
        apple.newApple();
    }
    

    /**
     Checks if the snake's head collides with any other part of its body.
     @return true if the snake's head collides with any other part of its body, false otherwise
     */
    

    public boolean checkSelfCollision() {
        CellPosition head = snake.getSnakeBody().get(0);
        return snake.getSnakeBody().stream()
                    .skip(1) // Skip the head
                    .anyMatch(segment -> positionsOverlap(head, segment));
    }
    
    private boolean positionsOverlap(CellPosition position1, CellPosition position2) {
        return position1.col() == position2.col() && position1.row() == position2.row();
    }
    

    /**
     * Sets the current game screen state.
     * @param gameScreen the game screen state to set
     */
    public void setGameScreen(PlayState gameScreen) {
        this.gameScreen = gameScreen;
    }

    /**
     * Returns the current game screen state.
     * @return the current game screen state
     */
    public PlayState getGameState () {
        return gameScreen;
    }

    /**
     * Returns the current apple object.
     * @return the current apple object.
     */
    public Apple getApple() {
        return apple;
    }
}
