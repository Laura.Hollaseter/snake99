package no.uib.inf101.sem2.GameLogic;



import no.uib.inf101.sem2.Grid.CellPosition;
import no.uib.inf101.sem2.Grid.Grid;


/**
 * Constructs a new SnakeBoard with the specified number of rows and columns.
 * Each cell of the board is initialized with the character '-'.
 * @param rows the number of rows for the board
 * @param cols the number of columns for the board
 */
public class GameArena extends Grid<Character> {
    public GameArena(int rows, int cols) {
        super(rows, cols,'-');
    }

    }


